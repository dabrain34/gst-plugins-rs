include: "https://gitlab.freedesktop.org/gstreamer/gst-ci/raw/master/gitlab/ci_template.yml"

.tarball_setup:
  variables:
    # Only stuff inside the repo directory can be cached
    # Override the CARGO_HOME variable to force its location
    CARGO_HOME: "${CI_PROJECT_DIR}/.cargo_home"
    SODIUM_USE_PKG_CONFIG: "true"
    DEPENDENCIES: |
      clang
      curl
      file
      git
      libsodium-dev
      libssl-dev
      liborc-0.4-dev
      libglib2.0-dev
      libgdk-pixbuf2.0-dev
      libgtk-3-dev
      libxml2-dev
      libegl1-mesa
      libgles2-mesa
      libgl1-mesa-dri
      libgl1-mesa-glx
      libwayland-egl1-mesa
      llvm
      nasm
      python3-pip
      python3-setuptools
      python3-wheel
      xz-utils
  before_script:
    - apt-get update -yqq
    - apt-get install -yqq --no-install-recommends $DEPENDENCIES
    - pip3 install --user meson ninja
    - export PATH="$PATH:$HOME/.local/bin"

    - mkdir -p precompiled-gst && cd precompiled-gst

    - curl -L https://people.freedesktop.org/~slomo/gstreamer-1.16.1.tar.xz | tar xJ
    - sed -i "s;prefix=/root/gstreamer;prefix=$PWD/gstreamer;g" $PWD/gstreamer/lib/x86_64-linux-gnu/pkgconfig/*.pc
    - export PKG_CONFIG_PATH=$PWD/gstreamer/lib/x86_64-linux-gnu/pkgconfig
    - export GST_PLUGIN_SYSTEM_PATH=$PWD/gstreamer/lib/x86_64-linux-gnu/gstreamer-1.0
    - export GST_PLUGIN_SCANNER=$PWD/gstreamer/libexec/gstreamer-1.0/gst-plugin-scanner
    - export PATH=$PATH:$PWD/gstreamer/bin
    - export LD_LIBRARY_PATH=$PWD/gstreamer/lib/x86_64-linux-gnu:$LD_LIBRARY_PATH
    # The build feature is required for the dav1d plugin for building and
    # statically linking the C dav1d library into the dav1d-rs bindings.
    # FIXME: The feature name should explicitly mention the dav1d plugin but
    # Cargo currently doesn't support passthrough for that scenario.
    - export RUSTFLAGS='--cfg feature="build"'

    - cd "${CI_PROJECT_DIR}"
  cache:
    key: "gst"
    paths:
      - "${CARGO_HOME}"

.cargo test:
  extends: '.tarball_setup'
  stage: "build"
  variables:
    GIT_STRATEGY: 'fetch'
  script:
    - rustc --version
    - cargo build --color=always --all
    - G_DEBUG=fatal_warnings cargo test --color=always --all
    - cargo build --color=always --all --examples --all-features
    - G_DEBUG=fatal_warnings cargo test --color=always --all --examples --all-features

test 1.40:
  # 1.40 img
  # https://hub.docker.com/_/rust/
  image: "rust:1.40-slim-buster"
  extends: '.cargo test'

test stable:
  # Stable img
  # https://hub.docker.com/_/rust/
  image: "rust:slim-buster"
  extends: '.cargo test'

test nightly:
  # Nightly
  # https://hub.docker.com/r/rustlang/rust/
  image: "rustlang/rust:nightly-buster-slim"
  allow_failure: true
  extends: '.cargo test'

rustfmt:
  image: "rust:slim-buster"
  stage: "preparation"
  variables:
    GIT_STRATEGY: 'fetch'
  script:
    - rustup component add rustfmt
    - cargo fmt --version
    - cargo fmt -- --color=always --check

clippy:
  extends: '.tarball_setup'
  image: "rust:slim-buster"
  stage: 'test'
  variables:
    GIT_STRATEGY: 'fetch'
  script:
    - rustup component add clippy-preview
    - cargo clippy --color=always --all --all-features --all-targets -- -A clippy::redundant_pattern_matching -A clippy::single_match -A clippy::cast_lossless

audit:
  extends: '.tarball_setup'
  image: "rust:slim-buster"
  stage: 'test'
  variables:
    GIT_STRATEGY: 'fetch'
  only:
    - schedules
  script:
    - cargo install --force cargo-audit
    - cargo audit --deny-warnings
